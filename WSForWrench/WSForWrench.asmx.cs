﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WSForWrench
{
    /// <summary>
    /// WSForWrench 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://EPMS.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    // [System.Web.Script.Services.ScriptService]
    public class WSForWrench : System.Web.Services.WebService
    {
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        /// <summary>
        /// 测试写入日志
        /// </summary>
        /// <param name="LogData"></param>
        [WebMethod]
        public void WriteLogTest(string LogData)
        {
            FileTrans.writeLog(LogData);
        }

        /// <summary>
        /// 测试接收文件
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="destFilePath"></param>
        /// <param name="fileName"></param>
        [WebMethod]
        public void GetFileFormWSTest(string filePath, string destFilePath, string fileName)
        {
            byte[] buffer = FileTrans.ConvertToBinary(filePath);
            FileTrans.SaveFile(buffer, destFilePath, fileName);
        }
        
        [WebMethod]
        public void GetDataFromDBTest(string Name)
        {
            DataOpt.GetData(Name);
        }
    }
}
