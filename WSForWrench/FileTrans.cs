﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace WSForWrench
{
    public class FileTrans
    {       
        #region  文件写入
        public static void SaveFile(byte[] buffer, string destFilePath, string fileName)
        {
            int index = buffer.Length / 20971520;//20971520bite就是20M,1*1024*1024*20
            index += buffer.Length % 20971520 == 0 ? 0 : 1;
            bool ifEnd = false;//是否为最后一组
            for (int ii = 0; ii < index; ii++)
            {
                if (ii == index - 1)
                    ifEnd = true;
                //Console.WriteLine("ConvertToBinary end at " + DateTime.Now.ToString());
                writeLog("Trans start at " + DateTime.Now.ToString());
                string rst = TransFile(ConvertToBinary(buffer,ii, ifEnd), destFilePath, fileName);//分批传输数据，追加到XXXX.XXX文件，如果不存在会自动创建
                writeLog(rst);
                writeLog("Trans end at " + DateTime.Now.ToString());
            }
            //Console.ReadLine();
        }
        private static string TransFile(byte[] fileBt, string destFilePath, string fileName)
        {
            string rst = "successful";
            if (fileBt.Length == 0)
                return "The length of the File" + fileName + "is 0";
            //string filePath = System.Configuration.ConfigurationManager.AppSettings["CSVPath"].ToString();   //文件路径
            //FileStream fstream = File.Create(filePath + fileName, fileBt.Length);
            //FileStream fstream = File.AppendAllText(
            FileStream fstream = new FileStream(destFilePath + fileName, FileMode.Append);
            try
            {
                //MemoryStream m = new MemoryStream(fileBt);
                //m.WriteTo(fstream);
                fstream.Write(fileBt, 0, fileBt.Length);   //二进制转换成文件
                rst += "File Name is:" + fstream.Name + "\r\n";
                rst += "File Length is:" + fstream.Length + "\r\n";
                //rst += "File Position is:" + fstream.Position + "\r\n";
                fstream.Close();
                //rst += "\r\n";
               
            }
            catch (Exception ex)
            {
                //抛出异常信息
                rst = ex.ToString();
            }
            finally
            {

                fstream.Close();
            }
            StringBuilder sbd = new StringBuilder();
            sbd.AppendLine(rst);
            return sbd.ToString();
        }

        public static byte[] ConvertToBinary(string Path)
        {
            FileStream stream = new FileInfo(Path).OpenRead();
            byte[] buffer = new byte[stream.Length];
            writeLog("The lenght of the file is " + buffer.Length);
            stream.Read(buffer, 0, Convert.ToInt32(stream.Length));
            stream.Close();
            return buffer;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index">是第几批</param>
        /// <param name="ifEnd"></param>
        /// <returns></returns>
        private static byte[] ConvertToBinary(byte[] buffer, int index, bool ifEnd)
        {
            //20971520
            //index = buffer.Length / 20971520;
            //index += buffer.Length % 20971520 == 0 ? 0 : 1;
            byte[] bys;//临时二进制数组，最大20M
            if (ifEnd == false)
            {
                bys = new byte[20971520];
                for (int ii = 20971520 * index; ii < 20971520 * (index + 1); ii++)
                {
                    bys[ii - 20971520 * index] = buffer[ii];
                }
            }
            else
            {
                bys = new byte[buffer.Length - 20971520 * (index)];
                for (int ii = 20971520 * index; ii < buffer.Length; ii++)
                {
                    bys[ii - 20971520 * index] = buffer[ii];
                }
            }
            writeLog("The length of the current buffer is " + bys.Length);
            return bys;

        }
        public static byte[] ChangeFileToByte(string path)
        {
            FileStream stream = new FileInfo(path).OpenRead();
            byte[] Filebuffer = new byte[stream.Length];
            stream.Read(Filebuffer, 0, Convert.ToInt32(stream.Length));
            return Filebuffer;
        }

        #endregion

        #region 写日志
        /// <summary>
        /// 写入文件日志
        /// </summary>
        /// <param name="LogData"></param>
        public static void writeLog(string LogData)
        {
            //创建文件夹
            string str = System.AppDomain.CurrentDomain.BaseDirectory;
            string newPath = System.IO.Path.Combine(str, "Log");
            if (!Directory.Exists(newPath))
            {
                System.IO.Directory.CreateDirectory(newPath);
            }

            string fileNameOne = DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            string filePathOne = System.IO.Path.Combine(newPath, fileNameOne);
            if (!File.Exists(filePathOne))
            {
                File.Create(filePathOne).Close();
            }
            WriteData(LogData, filePathOne);
        }

        private static void WriteData(string LogData, string fileName)
        {
            StreamWriter sw = new StreamWriter(fileName, true, Encoding.Default);
            sw.Write(DateTime.Now.ToString() + "  ");
            sw.WriteLine(LogData);
            sw.Flush();
            sw.Close();
        }
        #endregion
    }

}
